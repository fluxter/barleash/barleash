<?php

namespace App\Factory\Abstraction;

use App\Entity\Bar;
use Zenstruck\Foundry\Proxy;

trait BarRelatedEntityFactoryTrait
{
    public function withBar(Bar|Proxy $bar): self
    {
        return $this->addState(['bar' => $bar]);
    }
}
