<?php

namespace App\Factory;

use App\Entity\Bar;
use Doctrine\ORM\EntityRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Bar>
 *
 * @method        Bar|Proxy                        create(array|callable $attributes = [])
 * @method static Bar|Proxy                        createOne(array $attributes = [])
 * @method static Bar|Proxy                        find(object|array|mixed $criteria)
 * @method static Bar|Proxy                        findOrCreate(array $attributes)
 * @method static Bar|Proxy                        first(string $sortedField = 'id')
 * @method static Bar|Proxy                        last(string $sortedField = 'id')
 * @method static Bar|Proxy                        random(array $attributes = [])
 * @method static Bar|Proxy                        randomOrCreate(array $attributes = [])
 * @method static EntityRepository|RepositoryProxy repository()
 * @method static Bar[]|Proxy[]                    all()
 * @method static Bar[]|Proxy[]                    createMany(int $number, array|callable $attributes = [])
 * @method static Bar[]|Proxy[]                    createSequence(iterable|callable $sequence)
 * @method static Bar[]|Proxy[]                    findBy(array $attributes)
 * @method static Bar[]|Proxy[]                    randomRange(int $min, int $max, array $attributes = [])
 * @method static Bar[]|Proxy[]                    randomSet(int $number, array $attributes = [])
 */
final class BarFactory extends ModelFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     *
     * @todo inject services if required
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function getDefaults(): array
    {
        return [
            'name' => self::faker()->text(64),
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): self
    {
        return $this
            // ->afterInstantiate(function(Bar $bar): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Bar::class;
    }
}
