<?php

namespace App\Factory;

use App\Entity\RecipeCategory;
use App\Factory\Abstraction\BarRelatedEntityFactoryTrait;
use Doctrine\ORM\EntityRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<RecipeCategory>
 *
 * @method        RecipeCategory|Proxy             create(array|callable $attributes = [])
 * @method static RecipeCategory|Proxy             createOne(array $attributes = [])
 * @method static RecipeCategory|Proxy             find(object|array|mixed $criteria)
 * @method static RecipeCategory|Proxy             findOrCreate(array $attributes)
 * @method static RecipeCategory|Proxy             first(string $sortedField = 'id')
 * @method static RecipeCategory|Proxy             last(string $sortedField = 'id')
 * @method static RecipeCategory|Proxy             random(array $attributes = [])
 * @method static RecipeCategory|Proxy             randomOrCreate(array $attributes = [])
 * @method static EntityRepository|RepositoryProxy repository()
 * @method static RecipeCategory[]|Proxy[]         all()
 * @method static RecipeCategory[]|Proxy[]         createMany(int $number, array|callable $attributes = [])
 * @method static RecipeCategory[]|Proxy[]         createSequence(iterable|callable $sequence)
 * @method static RecipeCategory[]|Proxy[]         findBy(array $attributes)
 * @method static RecipeCategory[]|Proxy[]         randomRange(int $min, int $max, array $attributes = [])
 * @method static RecipeCategory[]|Proxy[]         randomSet(int $number, array $attributes = [])
 */
final class RecipeCategoryFactory extends ModelFactory
{
    use BarRelatedEntityFactoryTrait;

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     *
     * @todo inject services if required
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function getDefaults(): array
    {
        return [
            'bar' => BarFactory::new(),
            'name' => self::faker()->text(128),
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): self
    {
        return $this
            // ->afterInstantiate(function(RecipeCategory $recipeCategory): void {})
        ;
    }

    protected static function getClass(): string
    {
        return RecipeCategory::class;
    }
}
