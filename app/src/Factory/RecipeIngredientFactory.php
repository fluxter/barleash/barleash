<?php

namespace App\Factory;

use App\Entity\RecipeIngredient;
use App\Factory\Abstraction\BarRelatedEntityFactoryTrait;
use Doctrine\ORM\EntityRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<RecipeIngredient>
 *
 * @method        RecipeIngredient|Proxy           create(array|callable $attributes = [])
 * @method static RecipeIngredient|Proxy           createOne(array $attributes = [])
 * @method static RecipeIngredient|Proxy           find(object|array|mixed $criteria)
 * @method static RecipeIngredient|Proxy           findOrCreate(array $attributes)
 * @method static RecipeIngredient|Proxy           first(string $sortedField = 'id')
 * @method static RecipeIngredient|Proxy           last(string $sortedField = 'id')
 * @method static RecipeIngredient|Proxy           random(array $attributes = [])
 * @method static RecipeIngredient|Proxy           randomOrCreate(array $attributes = [])
 * @method static EntityRepository|RepositoryProxy repository()
 * @method static RecipeIngredient[]|Proxy[]       all()
 * @method static RecipeIngredient[]|Proxy[]       createMany(int $number, array|callable $attributes = [])
 * @method static RecipeIngredient[]|Proxy[]       createSequence(iterable|callable $sequence)
 * @method static RecipeIngredient[]|Proxy[]       findBy(array $attributes)
 * @method static RecipeIngredient[]|Proxy[]       randomRange(int $min, int $max, array $attributes = [])
 * @method static RecipeIngredient[]|Proxy[]       randomSet(int $number, array $attributes = [])
 */
final class RecipeIngredientFactory extends ModelFactory
{
    use BarRelatedEntityFactoryTrait;

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     *
     * @todo inject services if required
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function getDefaults(): array
    {
        return [
            'bar' => BarFactory::new(),
            'recipe' => RecipeFactory::new(),
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): self
    {
        return $this
            // ->afterInstantiate(function(RecipeIngredient $recipeIngredient): void {})
        ;
    }

    protected static function getClass(): string
    {
        return RecipeIngredient::class;
    }
}
