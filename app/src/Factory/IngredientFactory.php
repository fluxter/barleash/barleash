<?php

namespace App\Factory;

use App\Entity\Ingredient;
use App\Enum\UnitTypeEnum;
use App\Factory\Abstraction\BarRelatedEntityFactoryTrait;
use Doctrine\ORM\EntityRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Ingredient>
 *
 * @method        Ingredient|Proxy                 create(array|callable $attributes = [])
 * @method static Ingredient|Proxy                 createOne(array $attributes = [])
 * @method static Ingredient|Proxy                 find(object|array|mixed $criteria)
 * @method static Ingredient|Proxy                 findOrCreate(array $attributes)
 * @method static Ingredient|Proxy                 first(string $sortedField = 'id')
 * @method static Ingredient|Proxy                 last(string $sortedField = 'id')
 * @method static Ingredient|Proxy                 random(array $attributes = [])
 * @method static Ingredient|Proxy                 randomOrCreate(array $attributes = [])
 * @method static EntityRepository|RepositoryProxy repository()
 * @method static Ingredient[]|Proxy[]             all()
 * @method static Ingredient[]|Proxy[]             createMany(int $number, array|callable $attributes = [])
 * @method static Ingredient[]|Proxy[]             createSequence(iterable|callable $sequence)
 * @method static Ingredient[]|Proxy[]             findBy(array $attributes)
 * @method static Ingredient[]|Proxy[]             randomRange(int $min, int $max, array $attributes = [])
 * @method static Ingredient[]|Proxy[]             randomSet(int $number, array $attributes = [])
 */
final class IngredientFactory extends ModelFactory
{
    use BarRelatedEntityFactoryTrait;

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     *
     * @todo inject services if required
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function getDefaults(): array
    {
        return [
            'alcoholPercent' => self::faker()->randomNumber(),
            'name' => self::faker()->randomElement([
                'Rum', 'Tequila', 'Vodka', 'Wodka', 'Mandelsirup', 'Orangensaft', 'Grenadine', 'Zitronensaft',
                'Zucker', 'Eiswürfel', 'Cola', 'Gin', 'Whiskey', 'Cointreau', 'Sahne', 'Eiweiß', 'Eigelb', 'Zimt',
            ]),
            'unit' => self::faker()->randomElement(UnitTypeEnum::cases()),
            'stock' => self::faker()->randomNumber(),
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): self
    {
        return $this// ->afterInstantiate(function(Ingredient $ingredient): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Ingredient::class;
    }
}
