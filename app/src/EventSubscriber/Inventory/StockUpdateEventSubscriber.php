<?php

namespace App\EventSubscriber\Inventory;

use App\Entity\Order;
use App\Enum\OrderStatusEnum;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Events;

#[AsDoctrineListener(event: Events::onFlush)]
class StockUpdateEventSubscriber
{
    public function __invoke(OnFlushEventArgs $args): void
    {
        /** @var EntityManagerInterface $em */
        $em = $args->getObjectManager();
        $uow = $em->getUnitOfWork();
        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if (!$entity instanceof Order) {
                continue;
            }

            $changeSet = $uow->getEntityChangeSet($entity);
            if (!isset($changeSet['status'])) {
                continue;
            }

            if (OrderStatusEnum::Completed !== $entity->getStatus()) {
                continue;
            }

            foreach ($entity->getOrderItems() as $orderItem) {
                foreach ($orderItem->getRecipe()->getIngredients() as $recipeIngredient) {
                    $ingredient = $recipeIngredient->getIngredient();
                    $ingredient->setStock($ingredient->getStock() - ($recipeIngredient->getQuantity() * $orderItem->getQuantity()));
                    $em->persist($ingredient);
                    $uow->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($ingredient)), $ingredient);
                }
            }
        }
    }
}
