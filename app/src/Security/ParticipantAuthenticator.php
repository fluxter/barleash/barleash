<?php

namespace App\Security;

use App\Repository\ParticipantRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;

class ParticipantAuthenticator extends AbstractAuthenticator
{
    public function __construct(
        private readonly ParticipantRepository $participantRepository
    ) {
    }

    public function supports(Request $request): ?bool
    {
        return $request->headers->has('X-PARTICIPANT-TOKEN');
    }

    public function authenticate(Request $request): Passport
    {
        $participantToken = $request->headers->get('X-PARTICIPANT-TOKEN');
        if (null === $participantToken) {
            throw new CustomUserMessageAuthenticationException('No Participant token provided');
        }

        $user = $this->participantRepository->find($participantToken);
        if (null === $user) {
            throw new CustomUserMessageAuthenticationException('invalid Participant token provided');
        }

        if (!$user->isClaimed()) {
            $user->setClaimed(true);
            $this->participantRepository->save($user);
        }

        return new SelfValidatingPassport(new UserBadge($user->getId()));
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        // on success, let the request continue
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $data = [
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData()),
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }
}
