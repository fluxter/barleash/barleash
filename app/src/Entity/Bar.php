<?php

namespace App\Entity;

use ApiPlatform\Metadata as Api;
use App\Entity\Abstract\AbstractEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[Api\ApiResource(
    operations: [
        new Api\Get(name: 'getBar'),
        new Api\GetCollection(normalizationContext: ['groups' => [self::List]], name: 'getBars'),
        new Api\Post(denormalizationContext: ['groups' => [self::Create]], name: 'createBar'),
        new Api\Patch(denormalizationContext: ['groups' => [self::Update]], name: 'updateBar'),
    ],
    normalizationContext: ['groups' => [self::Read]],
)]
#[ORM\Entity]
class Bar extends AbstractEntity
{
    private const Prefix = 'bar';
    private const Read = self::Prefix.self::ReadSuffix;
    private const Create = self::Prefix.self::CreateSuffix;
    private const List = self::Prefix.self::ListSuffix;
    private const Update = self::Prefix.self::UpdateSuffix;

    #[ORM\Column(length: 64)]
    #[Groups([self::List, self::Read, self::Create, self::Update])]
    #[Assert\Length(min: 3, max: 64)]
    private string $name = '';

    #[ORM\OneToMany(mappedBy: 'bar', targetEntity: Ingredient::class)]
    private Collection $ingredients;

    #[ORM\OneToMany(mappedBy: 'bar', targetEntity: Recipe::class)]
    private Collection $recipes;

    #[ORM\OneToMany(mappedBy: 'bar', targetEntity: Order::class)]
    private Collection $orders;

    #[ORM\OneToMany(mappedBy: 'bar', targetEntity: RecipeIngredient::class)]
    private Collection $recipeIngredients;

    #[ORM\OneToMany(mappedBy: 'bar', targetEntity: OrderItem::class)]
    private Collection $orderItems;

    #[ORM\OneToMany(mappedBy: 'bar', targetEntity: Event::class)]
    private Collection $events;

    #[ORM\OneToMany(mappedBy: 'bar', targetEntity: Participant::class)]
    private Collection $participants;

    public function __construct()
    {
        parent::__construct();
        $this->ingredients = new ArrayCollection();
        $this->recipes = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->recipeIngredients = new ArrayCollection();
        $this->orderItems = new ArrayCollection();
        $this->events = new ArrayCollection();
        $this->participants = new ArrayCollection();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Ingredient>
     */
    public function getIngredients(): Collection
    {
        return $this->ingredients;
    }

    public function addIngredient(Ingredient $ingredient): static
    {
        if (!$this->ingredients->contains($ingredient)) {
            $this->ingredients->add($ingredient);
            $ingredient->setBar($this);
        }

        return $this;
    }

    public function removeIngredient(Ingredient $ingredient): static
    {
        if ($this->ingredients->removeElement($ingredient)) {
            // set the owning side to null (unless already changed)
            if ($ingredient->getBar() === $this) {
                $ingredient->setBar(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Recipe>
     */
    public function getRecipes(): Collection
    {
        return $this->recipes;
    }

    public function addRecipe(Recipe $recipe): static
    {
        if (!$this->recipes->contains($recipe)) {
            $this->recipes->add($recipe);
            $recipe->setBar($this);
        }

        return $this;
    }

    public function removeRecipe(Recipe $recipe): static
    {
        if ($this->recipes->removeElement($recipe)) {
            // set the owning side to null (unless already changed)
            if ($recipe->getBar() === $this) {
                $recipe->setBar(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Order>
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): static
    {
        if (!$this->orders->contains($order)) {
            $this->orders->add($order);
            $order->setBar($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): static
    {
        if ($this->orders->removeElement($order)) {
            // set the owning side to null (unless already changed)
            if ($order->getBar() === $this) {
                $order->setBar(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, RecipeIngredient>
     */
    public function getRecipeIngredients(): Collection
    {
        return $this->recipeIngredients;
    }

    public function addRecipeIngredient(RecipeIngredient $recipeIngredient): static
    {
        if (!$this->recipeIngredients->contains($recipeIngredient)) {
            $this->recipeIngredients->add($recipeIngredient);
            $recipeIngredient->setBar($this);
        }

        return $this;
    }

    public function removeRecipeIngredient(RecipeIngredient $recipeIngredient): static
    {
        if ($this->recipeIngredients->removeElement($recipeIngredient)) {
            // set the owning side to null (unless already changed)
            if ($recipeIngredient->getBar() === $this) {
                $recipeIngredient->setBar(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, OrderItem>
     */
    public function getOrderItems(): Collection
    {
        return $this->orderItems;
    }

    public function addOrderItem(OrderItem $orderItem): static
    {
        if (!$this->orderItems->contains($orderItem)) {
            $this->orderItems->add($orderItem);
            $orderItem->setBar($this);
        }

        return $this;
    }

    public function removeOrderItem(OrderItem $orderItem): static
    {
        if ($this->orderItems->removeElement($orderItem)) {
            // set the owning side to null (unless already changed)
            if ($orderItem->getBar() === $this) {
                $orderItem->setBar(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Event>
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): static
    {
        if (!$this->events->contains($event)) {
            $this->events->add($event);
            $event->setBar($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): static
    {
        if ($this->events->removeElement($event)) {
            // set the owning side to null (unless already changed)
            if ($event->getBar() === $this) {
                $event->setBar(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Participant>
     */
    public function getParticipants(): Collection
    {
        return $this->participants;
    }

    public function addParticipant(Participant $participant): static
    {
        if (!$this->participants->contains($participant)) {
            $this->participants->add($participant);
            $participant->setBar($this);
        }

        return $this;
    }

    public function removeParticipant(Participant $participant): static
    {
        if ($this->participants->removeElement($participant)) {
            // set the owning side to null (unless already changed)
            if ($participant->getBar() === $this) {
                $participant->setBar(null);
            }
        }

        return $this;
    }
}
