<?php

namespace App\Entity;

use App\Entity\Abstract\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
class OrderItem extends AbstractEntity
{
    //    private const Prefix = "orderitem";
    //    private const Read = self::Prefix . self::ReadSuffix;
    //    private const Create = self::Prefix . self::CreateSuffix;
    //    private const List = self::Prefix . self::ListSuffix;
    //    private const Update = self::Prefix . self::UpdateSuffix;

    #[ORM\ManyToOne(inversedBy: 'orderItems')]
    #[Groups([Order::Read, Order::Create])]
    #[ORM\JoinColumn(nullable: false)]
    private Recipe $recipe;

    #[ORM\ManyToOne(inversedBy: 'orderItems')]
    #[ORM\JoinColumn(nullable: false)]
    private Order $order;

    #[ORM\Column(nullable: false)]
    #[Assert\Length(min: 1, max: 10)]
    #[Groups([Order::Read, Order::Create])]
    private int $quantity;

    #[ORM\Column(nullable: true)]
    #[Groups([Order::Read, Order::Create])]
    private ?string $notes;

    #[ORM\ManyToOne(targetEntity: Bar::class, inversedBy: 'orderItems')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Bar $bar;

    public function getRecipe(): ?Recipe
    {
        return $this->recipe;
    }

    public function setRecipe(?Recipe $recipe): static
    {
        $this->recipe = $recipe;

        return $this;
    }

    public function getOrder(): ?Order
    {
        return $this->order;
    }

    public function setOrder(?Order $order): static
    {
        $this->order = $order;

        return $this;
    }

    public function getBar(): ?Bar
    {
        return $this->bar;
    }

    public function setBar(?Bar $bar): static
    {
        $this->bar = $bar;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(string $notes): static
    {
        $this->notes = $notes;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): static
    {
        $this->quantity = $quantity;

        return $this;
    }
}
