<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata as Api;
use App\Controller\Participant\QrCodeController;
use App\Entity\Abstract\AbstractEntity;
use App\Repository\ParticipantRepository;
use App\State\ParticipantProcessor;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

#[Api\ApiResource(
    operations: [
        new Api\Get(name: 'getParticipant'),
        new Api\Get(
            name: 'getParticipantQrCode',
            uriTemplate: '/participants/{id}/qr-code',
            description: 'Show the participants login qr code',
            controller: QrCodeController::class,
            openapiContext: [
                'summary' => 'Show the participants login qr code',
                'description' => 'Show the participants login qr code',
                'responses' => [
                    '200' => [
                        'description' => 'The participants login qr code',
                        'content' => [
                            'image/png' => [
                                'schema' => [
                                    'type' => 'string',
                                    'format' => 'binary',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ),
        new Api\GetCollection(normalizationContext: ['groups' => [self::List]], name: 'getParticipants'),
        new Api\Post(denormalizationContext: ['groups' => [self::Create]], name: 'createParticipant'),
        new Api\Patch(denormalizationContext: ['groups' => [self::Update]], name: 'patchParticipant'),
    ],
    normalizationContext: ['groups' => [self::Read]],
    processor: ParticipantProcessor::class
)]
#[Api\ApiFilter(SearchFilter::class, properties: ['bar' => 'exact', 'event' => 'exact', 'name' => 'partial', 'claimed'])]
#[ORM\Entity(repositoryClass: ParticipantRepository::class)]
class Participant extends AbstractEntity implements UserInterface
{
    private const Prefix = 'participant';
    private const Read = self::Prefix.self::ReadSuffix;
    private const Create = self::Prefix.self::CreateSuffix;
    private const List = self::Prefix.self::ListSuffix;
    private const Update = self::Prefix.self::UpdateSuffix;

    #[ORM\Column(nullable: false)]
    #[Groups([self::Read, self::Update, self::List, Order::Read, Order::List])]
    private string $name = 'Pending...';

    #[ORM\Column(nullable: false)]
    #[Groups([self::Read, self::Update, self::List])]
    private bool $claimed = false;

    #[ORM\ManyToOne(targetEntity: Bar::class, inversedBy: 'participants')]
    #[ORM\JoinColumn(nullable: false)]
    private Bar $bar;

    #[ORM\ManyToOne(targetEntity: Event::class, inversedBy: 'participants')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups([self::Create])]
    private Event $event;

    #[ORM\OneToMany(mappedBy: 'participant', targetEntity: Order::class)]
    private Collection $orders;

    public function __construct()
    {
        parent::__construct();
        $this->orders = new ArrayCollection();
    }

    public function getRoles(): array
    {
        return [
            'ROLE_PARTICIPANT',
        ];
    }

    public function eraseCredentials()
    {
    }

    public function getUserIdentifier(): string
    {
        return $this->getId();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getBar(): ?Bar
    {
        return $this->bar;
    }

    public function setBar(?Bar $bar): static
    {
        $this->bar = $bar;

        return $this;
    }

    public function getEvent(): ?Event
    {
        return $this->event;
    }

    public function setEvent(?Event $event): static
    {
        $this->event = $event;

        return $this;
    }

    /**
     * @return Collection<int, Order>
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): static
    {
        if (!$this->orders->contains($order)) {
            $this->orders->add($order);
            $order->setParticipant($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): static
    {
        if ($this->orders->removeElement($order)) {
            // set the owning side to null (unless already changed)
            if ($order->getParticipant() === $this) {
                $order->setParticipant(null);
            }
        }

        return $this;
    }

    public function isClaimed(): ?bool
    {
        return $this->claimed;
    }

    public function setClaimed(bool $claimed): static
    {
        $this->claimed = $claimed;

        return $this;
    }
}
