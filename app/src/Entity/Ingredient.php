<?php

namespace App\Entity;

use ApiPlatform\Metadata as Api;
use App\Entity\Abstract\AbstractEntity;
use App\Enum\UnitTypeEnum;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[Api\ApiResource(
    operations: [
        new Api\Get(name: 'getIngredient'),
        new Api\GetCollection(normalizationContext: ['groups' => [self::List]], name: 'getIngredients'),
        new Api\Post(denormalizationContext: ['groups' => [self::Create]], name: 'createIngredient'),
        new Api\Patch(denormalizationContext: ['groups' => [self::Update]], name: 'updateIngredient'),
    ],
    normalizationContext: ['groups' => [self::Read]],
)]
#[ORM\Entity]
class Ingredient extends AbstractEntity
{
    private const Prefix = 'ingredient';
    private const Read = self::Prefix.self::ReadSuffix;
    private const Create = self::Prefix.self::CreateSuffix;
    private const List = self::Prefix.self::ListSuffix;
    private const Update = self::Prefix.self::UpdateSuffix;

    #[ORM\Column(type: Types::STRING, nullable: false)]
    #[Groups([self::List, self::Read, self::Create, self::Update, Order::Read])]
    private string $name = '';

    #[ORM\Column(type: Types::INTEGER, nullable: false)]
    #[Assert\Range(min: 0, max: 100)]
    private int $alcoholPercent = 0;

    #[ORM\Column(type: Types::INTEGER)]
    #[Groups([self::List, self::Read, self::Create, self::Update])]
    private int $stock = 0;

    #[ORM\Column(type: Types::STRING, nullable: false, enumType: UnitTypeEnum::class)]
    #[Groups([self::List, self::Read, self::Create, self::Update, Order::Read])]
    private UnitTypeEnum $unit;

    #[ORM\OneToMany(mappedBy: 'ingredient', targetEntity: RecipeIngredient::class)]
    private Collection $recipes;

    #[ORM\ManyToOne(inversedBy: 'ingredients')]
    #[Groups([self::Create])]
    #[Assert\NotBlank]
    private ?Bar $bar;

    public function __construct()
    {
        parent::__construct();
        $this->recipes = new ArrayCollection();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }
    
    /**
     * @return Collection<int, RecipeIngredient>
     */
    public function getRecipes(): Collection
    {
        return $this->recipes;
    }

    public function addRecipe(RecipeIngredient $recipe): static
    {
        if (!$this->recipes->contains($recipe)) {
            $this->recipes->add($recipe);
            $recipe->setIngredient($this);
        }

        return $this;
    }

    public function removeRecipe(RecipeIngredient $recipe): static
    {
        if ($this->recipes->removeElement($recipe)) {
            // set the owning side to null (unless already changed)
            if ($recipe->getIngredient() === $this) {
                $recipe->setIngredient(null);
            }
        }

        return $this;
    }

    public function getBar(): ?Bar
    {
        return $this->bar;
    }

    public function setBar(?Bar $bar): static
    {
        $this->bar = $bar;

        return $this;
    }

    public function getAlcoholPercent(): ?int
    {
        return $this->alcoholPercent;
    }

    public function setAlcoholPercent(int $alcoholPercent): static
    {
        $this->alcoholPercent = $alcoholPercent;

        return $this;
    }

    public function getUnit(): ?UnitTypeEnum
    {
        return $this->unit;
    }

    public function setUnit(UnitTypeEnum $unit): static
    {
        $this->unit = $unit;

        return $this;
    }

    public function getStock(): ?int
    {
        return $this->stock;
    }

    public function setStock(int $stock): static
    {
        $this->stock = $stock;

        return $this;
    }
}
