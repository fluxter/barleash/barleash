<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata as Api;
use App\Entity\Abstract\AbstractEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[Api\ApiResource(
    operations: [
        new Api\Get(name: 'getRecipe'),
        new Api\GetCollection(normalizationContext: ['groups' => [self::List]], name: 'getRecipes'),
        new Api\Post(denormalizationContext: ['groups' => [self::Create]], name: 'createRecipe'),
        new Api\Patch(denormalizationContext: ['groups' => [self::Update]], name: 'updateRecipe'),
    ],
    normalizationContext: ['groups' => [self::Read]],
)]
#[Api\ApiFilter(SearchFilter::class, properties: ['bar' => 'exact'])]
#[ORM\Entity]
class Recipe extends AbstractEntity
{
    private const Prefix = 'recipe';
    private const Read = self::Prefix.self::ReadSuffix;
    private const Create = self::Prefix.self::CreateSuffix;
    private const List = self::Prefix.self::ListSuffix;
    private const Update = self::Prefix.self::UpdateSuffix;

    #[ORM\Column(length: 128, nullable: false)]
    #[Groups([self::List, self::Read, self::Create, self::Update, RecipeCategory::Read, RecipeCategory::List, Order::Read])]
    #[Assert\Length(min: 3, max: 128)]
    private string $name;

    #[ORM\Column(nullable: true)]
    #[Groups([self::List, self::Read, self::Create, self::Update])]
    #[Assert\Length(max: 255)]
    private ?string $description;

    #[ORM\OneToMany(mappedBy: 'recipe', targetEntity: RecipeIngredient::class)]
    #[Groups([self::Read, self::Create, self::Update, Order::Read])]
    private Collection $ingredients;

    #[ORM\OneToMany(mappedBy: 'recipe', targetEntity: OrderItem::class)]
    private Collection $orderItems;

    #[ORM\ManyToOne(targetEntity: RecipeCategory::class, inversedBy: 'recipes')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups([self::Create, self::Update, self::List, self::Read])]
    private RecipeCategory $recipeCategory;

    #[ORM\ManyToOne(targetEntity: Bar::class, inversedBy: 'recipes')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups([self::Create])]
    private Bar $bar;

    public function __construct()
    {
        parent::__construct();
        $this->ingredients = new ArrayCollection();
        $this->orderItems = new ArrayCollection();
    }

    #[Groups([self::List])]
    public function getOrderedCount(): int
    {
        return $this->orderItems->count();
    }

    #[Groups([self::Read, self::List])]
    public function isAlcoholic(): bool
    {
        return $this->ingredients->filter(fn (RecipeIngredient $i) => 0 != $i->getIngredient()->getAlcoholPercent())->count();
    }

    #[Groups([self::Read, self::List])]
    public function getAvailableCount(): int
    {
        return random_int(0, 10);
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, RecipeIngredient>
     */
    public function getIngredients(): Collection
    {
        return $this->ingredients;
    }

    public function addIngredient(RecipeIngredient $ingredient): static
    {
        if (!$this->ingredients->contains($ingredient)) {
            $this->ingredients->add($ingredient);
            $ingredient->setRecipe($this);
        }

        return $this;
    }

    public function removeIngredient(RecipeIngredient $ingredient): static
    {
        if ($this->ingredients->removeElement($ingredient)) {
            // set the owning side to null (unless already changed)
            if ($ingredient->getRecipe() === $this) {
                $ingredient->setRecipe(null);
            }
        }

        return $this;
    }

    public function getBar(): ?Bar
    {
        return $this->bar;
    }

    public function setBar(?Bar $bar): static
    {
        $this->bar = $bar;

        return $this;
    }

    /**
     * @return Collection<int, OrderItem>
     */
    public function getOrderItems(): Collection
    {
        return $this->orderItems;
    }

    public function addOrderItem(OrderItem $orderItem): static
    {
        if (!$this->orderItems->contains($orderItem)) {
            $this->orderItems->add($orderItem);
            $orderItem->setRecipe($this);
        }

        return $this;
    }

    public function removeOrderItem(OrderItem $orderItem): static
    {
        if ($this->orderItems->removeElement($orderItem)) {
            // set the owning side to null (unless already changed)
            if ($orderItem->getRecipe() === $this) {
                $orderItem->setRecipe(null);
            }
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getRecipeCategory(): ?RecipeCategory
    {
        return $this->recipeCategory;
    }

    public function setRecipeCategory(?RecipeCategory $recipeCategory): static
    {
        $this->recipeCategory = $recipeCategory;

        return $this;
    }
}
