<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata as Api;
use App\Entity\Abstract\AbstractEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[Api\ApiResource(
    operations: [
        new Api\Get(name: 'getRecipeCategories'),
        new Api\GetCollection(normalizationContext: ['groups' => [self::List]], name: 'getRecipesCategory'),
        new Api\Post(denormalizationContext: ['groups' => [self::Create]], name: 'createRecipeCategory'),
        new Api\Patch(denormalizationContext: ['groups' => [self::Update]], name: 'updateRecipeCategory'),
    ],
    normalizationContext: ['groups' => [self::Read]],
)]
#[Api\ApiFilter(SearchFilter::class, properties: [])]
#[ORM\Entity]
class RecipeCategory extends AbstractEntity
{
    private const Prefix = 'recipeCategory';
    public const Read = self::Prefix.self::ReadSuffix;
    public const Create = self::Prefix.self::CreateSuffix;
    public const List = self::Prefix.self::ListSuffix;
    public const Update = self::Prefix.self::UpdateSuffix;

    #[ORM\Column(length: 128, nullable: false)]
    #[Groups([self::List, self::Read, self::Create, self::Update, Order::Read])]
    #[Assert\Length(min: 3, max: 128)]
    private string $name;

    #[ORM\Column(nullable: true)]
    #[Groups([self::List, self::Read, self::Create, self::Update])]
    #[Assert\Length(max: 255)]
    private ?string $description;

    #[ORM\OneToMany(mappedBy: 'recipeCategory', targetEntity: Recipe::class)]
    #[Groups([self::Read, self::Create, self::Update, Order::Read])]
    private Collection $recipes;

    #[ORM\ManyToOne(targetEntity: Bar::class, inversedBy: 'recipes')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups([self::Create])]
    private Bar $bar;

    public function __construct()
    {
        parent::__construct();
        $this->recipes = new ArrayCollection();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, Recipe>
     */
    public function getRecipes(): Collection
    {
        return $this->recipes;
    }

    public function addRecipe(Recipe $recipe): static
    {
        if (!$this->recipes->contains($recipe)) {
            $this->recipes->add($recipe);
            $recipe->setRecipeCategory($this);
        }

        return $this;
    }

    public function removeRecipe(Recipe $recipe): static
    {
        if ($this->recipes->removeElement($recipe)) {
            // set the owning side to null (unless already changed)
            if ($recipe->getRecipeCategory() === $this) {
                $recipe->setRecipeCategory(null);
            }
        }

        return $this;
    }

    public function getBar(): ?Bar
    {
        return $this->bar;
    }

    public function setBar(?Bar $bar): static
    {
        $this->bar = $bar;

        return $this;
    }
}
