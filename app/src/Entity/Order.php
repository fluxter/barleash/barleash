<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata as Api;
use App\Entity\Abstract\AbstractEntity;
use App\Enum\OrderStatusEnum;
use App\Repository\OrderRepository;
use App\State\OrderProcessor;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[Api\ApiResource(
    operations: [
        new Api\Get(name: 'getOrder'),
        new Api\GetCollection(normalizationContext: ['groups' => [self::List]], name: 'getOrders'),
        new Api\Post(
            denormalizationContext: ['groups' => [self::Create]],
            name: 'createOrder',
            security: "is_granted('ROLE_PARTICIPANT')"
        ),
        new Api\Patch(denormalizationContext: ['groups' => [self::Update]], name: 'patchOrder'),
    ],
    normalizationContext: ['groups' => [self::Read]],
    processor: OrderProcessor::class
)]
#[Api\ApiFilter(SearchFilter::class, properties: ['bar' => 'exact', 'status', 'participant'])]
#[Api\ApiFilter(OrderFilter::class, properties: ['createdAt'])]
#[ORM\Entity(repositoryClass: OrderRepository::class)]
#[ORM\Table('"order"')]
class Order extends AbstractEntity
{
    private const Prefix = 'order';
    public const Read = self::Prefix.self::ReadSuffix;
    public const Create = self::Prefix.self::CreateSuffix;
    public const List = self::Prefix.self::ListSuffix;
    public const Update = self::Prefix.self::UpdateSuffix;

    #[ORM\Column(type: Types::INTEGER, nullable: false)]
    #[Groups([self::Read, self::List])]
    private int $orderNumber = 0;

    #[ORM\Column(nullable: false, enumType: OrderStatusEnum::class)]
    #[Groups([self::Read, self::Update])]
    private OrderStatusEnum $status = OrderStatusEnum::Pending;

    #[ORM\OneToMany(mappedBy: 'order', targetEntity: OrderItem::class, cascade: ['persist', 'remove'], orphanRemoval: true)]
    #[Groups([self::Read, self::Create])]
    private Collection $orderItems;

    #[ORM\ManyToOne(targetEntity: Event::class, inversedBy: 'orders')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Event $event = null;

    #[ORM\ManyToOne(targetEntity: Participant::class, inversedBy: 'orders')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups([self::List, self::Read])]
    private Participant $participant;

    #[ORM\ManyToOne(targetEntity: Bar::class, inversedBy: 'orders')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Bar $bar;

    #[Groups([self::Read])]
    public function getCreatedAt(): \DateTimeInterface
    {
        return parent::getCreatedAt();
    }

    public function __construct()
    {
        parent::__construct();
        $this->orderItems = new ArrayCollection();
    }

    public function getBar(): ?Bar
    {
        return $this->bar;
    }

    public function setBar(?Bar $bar): static
    {
        $this->bar = $bar;

        return $this;
    }

    public function getOrderNumber(): ?int
    {
        return $this->orderNumber;
    }

    public function setOrderNumber(int $orderNumber): static
    {
        $this->orderNumber = $orderNumber;

        return $this;
    }

    public function getStatus(): ?OrderStatusEnum
    {
        return $this->status;
    }

    public function setStatus(OrderStatusEnum $status): static
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection<int, OrderItem>
     */
    public function getOrderItems(): Collection
    {
        return $this->orderItems;
    }

    public function addOrderItem(OrderItem $orderItem): static
    {
        if (!$this->orderItems->contains($orderItem)) {
            $this->orderItems->add($orderItem);
            $orderItem->setOrder($this);
        }

        return $this;
    }

    public function removeOrderItem(OrderItem $orderItem): static
    {
        if ($this->orderItems->removeElement($orderItem)) {
            // set the owning side to null (unless already changed)
            if ($orderItem->getOrder() === $this) {
                $orderItem->setOrder(null);
            }
        }

        return $this;
    }

    public function getEvent(): ?Event
    {
        return $this->event;
    }

    public function setEvent(?Event $event): static
    {
        $this->event = $event;

        return $this;
    }

    public function getParticipant(): ?Participant
    {
        return $this->participant;
    }

    public function setParticipant(?Participant $participant): static
    {
        $this->participant = $participant;

        return $this;
    }
}
