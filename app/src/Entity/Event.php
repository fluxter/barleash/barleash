<?php

namespace App\Entity;

use ApiPlatform\Metadata as Api;
use App\Entity\Abstract\AbstractEntity;
use App\Filter\EventActiveFilter;
use App\Repository\EventRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[Api\ApiResource(
    operations: [
        new Api\Get(name: 'getEvent'),
        new Api\GetCollection(normalizationContext: ['groups' => [self::List]], name: 'getEvents'),
        new Api\Post(denormalizationContext: ['groups' => [self::Create]], name: 'createEvent'),
        new Api\Patch(denormalizationContext: ['groups' => [self::Update]], name: 'patchEvent'),
    ],
    normalizationContext: ['groups' => [self::Read]],
)]
#[Api\ApiFilter(EventActiveFilter::class)]
#[ORM\Entity(repositoryClass: EventRepository::class)]
class Event extends AbstractEntity
{
    private const Prefix = 'event';
    private const Read = self::Prefix.self::ReadSuffix;
    private const Create = self::Prefix.self::CreateSuffix;
    private const List = self::Prefix.self::ListSuffix;
    private const Update = self::Prefix.self::UpdateSuffix;

    #[ORM\Column(nullable: false, length: 64)]
    #[Assert\Length(max: 64)]
    #[Groups(self::Read, self::List, self::Create, self::Update)]
    private string $name;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: false)]
    #[Assert\NotBlank()]
    #[Groups(self::Read, self::List, self::Create, self::Update)]
    private \DateTimeInterface $start;

    #[ORM\Column(name: '"end"', type: Types::DATE_MUTABLE)]
    #[Assert\NotBlank()]
    #[Groups(self::Read, self::List, self::Create, self::Update)]
    private \DateTimeInterface $end;

    #[ORM\OneToMany(mappedBy: 'event', targetEntity: Order::class)]
    private Collection $orders;

    #[ORM\ManyToOne(targetEntity: Bar::class, inversedBy: 'events')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(self::Create)]
    private ?Bar $bar;

    #[ORM\OneToMany(mappedBy: 'event', targetEntity: Participant::class)]
    private Collection $participants;

    #[Assert\Callback()]
    public function validate(): void
    {
        if ($this->start > $this->end) {
            throw new \InvalidArgumentException('Start date must be before end date');
        }
    }

    public function __construct()
    {
        parent::__construct();
        $this->orders = new ArrayCollection();
        $this->participants = new ArrayCollection();
    }

    #[Groups([self::Read, self::List])]
    public function isActive(): bool
    {
        $now = new \DateTime();

        return $this->start < $now && $this->end > $now;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getStart(): ?\DateTimeInterface
    {
        return $this->start;
    }

    public function setStart(\DateTimeInterface $start): static
    {
        $this->start = $start;

        return $this;
    }

    public function getEnd(): ?\DateTimeInterface
    {
        return $this->end;
    }

    public function setEnd(\DateTimeInterface $end): static
    {
        $this->end = $end;

        return $this;
    }

    /**
     * @return Collection<int, Order>
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): static
    {
        if (!$this->orders->contains($order)) {
            $this->orders->add($order);
            $order->setEvent($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): static
    {
        if ($this->orders->removeElement($order)) {
            // set the owning side to null (unless already changed)
            if ($order->getEvent() === $this) {
                $order->setEvent(null);
            }
        }

        return $this;
    }

    public function getBar(): ?Bar
    {
        return $this->bar;
    }

    public function setBar(?Bar $bar): static
    {
        $this->bar = $bar;

        return $this;
    }

    /**
     * @return Collection<int, Participant>
     */
    public function getParticipants(): Collection
    {
        return $this->participants;
    }

    public function addParticipant(Participant $participant): static
    {
        if (!$this->participants->contains($participant)) {
            $this->participants->add($participant);
            $participant->setEvent($this);
        }

        return $this;
    }

    public function removeParticipant(Participant $participant): static
    {
        if ($this->participants->removeElement($participant)) {
            // set the owning side to null (unless already changed)
            if ($participant->getEvent() === $this) {
                $participant->setEvent(null);
            }
        }

        return $this;
    }
}
