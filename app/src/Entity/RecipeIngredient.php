<?php

namespace App\Entity;

use App\Entity\Abstract\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
class RecipeIngredient extends AbstractEntity
{
    private const Prefix = 'recipeingredient';
    private const Read = self::Prefix.self::ReadSuffix;
    private const Create = self::Prefix.self::CreateSuffix;
    private const List = self::Prefix.self::ListSuffix;
    private const Update = self::Prefix.self::UpdateSuffix;

    #[ORM\Column(nullable: false)]
    #[Groups([Order::Read])]
    private int $quantity = 0;

    #[ORM\ManyToOne(inversedBy: 'ingredients')]
    #[ORM\JoinColumn(nullable: false)]
    private Recipe $recipe;

    #[ORM\ManyToOne(inversedBy: 'recipes')]
    #[Groups([self::List, self::Read, self::Create, self::Update, Order::Read])]
    private Ingredient $ingredient;

    #[ORM\ManyToOne(targetEntity: Bar::class, inversedBy: 'recipeIngredients')]
    #[ORM\JoinColumn(nullable: false)]
    private Bar $bar;

    public function getRecipe(): ?Recipe
    {
        return $this->recipe;
    }

    public function setRecipe(?Recipe $recipe): static
    {
        $this->recipe = $recipe;

        return $this;
    }

    public function getIngredient(): ?Ingredient
    {
        return $this->ingredient;
    }

    public function setIngredient(?Ingredient $ingredient): static
    {
        $this->ingredient = $ingredient;

        return $this;
    }

    public function getBar(): ?Bar
    {
        return $this->bar;
    }

    public function setBar(?Bar $bar): static
    {
        $this->bar = $bar;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): static
    {
        $this->quantity = $quantity;

        return $this;
    }
}
