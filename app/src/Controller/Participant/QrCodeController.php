<?php

namespace App\Controller\Participant;

use App\Entity\Participant;
use Endroid\QrCode\Builder\BuilderInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class QrCodeController
{
    public function __construct(
        private readonly string $frontendUrl,
        private readonly BuilderInterface $builder
    ) {
    }

    public function __invoke(Participant $participant): Response
    {
        $data = [
            'event' => $participant->getEvent()->getId()->toRfc4122(),
            'bar' => $participant->getBar()->getId()->toRfc4122(),
            'loginToken' => $participant->getId(),
        ];
        $url = sprintf('%s/login?%s', $this->frontendUrl, http_build_query($data));

        return new Response(
            content: $this->builder->data($url)->build()->getString(),
            headers: [
                'Content-Type' => 'image/png',
            ],
        );
    }
}
