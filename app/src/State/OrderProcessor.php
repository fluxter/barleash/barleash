<?php

namespace App\State;

use ApiPlatform\Metadata\DeleteOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\Metadata\Post;
use ApiPlatform\State\ProcessorInterface;
use App\Entity\Order;
use App\Entity\Participant;
use App\Repository\OrderRepository;
use Symfony\Bundle\SecurityBundle\Security;

final class OrderProcessor implements ProcessorInterface
{
    public function __construct(
        private readonly ProcessorInterface $persistProcessor,
        private readonly ProcessorInterface $removeProcessor,
        private readonly OrderRepository $orderRepository,
        private readonly Security $security
    ) {
    }

    /** @param Order $data */
    public function process($data, Operation $operation, array $uriVariables = [], array $context = [])
    {
        if ($operation instanceof DeleteOperationInterface) {
            return $this->removeProcessor->process($data, $operation, $uriVariables, $context);
        }

        if ($operation instanceof Post) {
            $user = $this->security->getUser();
            if (!$user instanceof Participant) {
                throw new \LogicException('Only bar can create order');
            }

            $bar = $user->getEvent()->getBar();
            $data
                ->setParticipant($user)
                ->setBar($bar)
                ->setEvent($user->getEvent())
                ->setOrderNumber($this->orderRepository->getNextOrderNumberByBar($user->getEvent()->getBar()))
            ;
            foreach ($data->getOrderItems() as $orderItem) {
                $orderItem->setBar($bar);
            }
        }

        $result = $this->persistProcessor->process($data, $operation, $uriVariables, $context);

        return $result;
    }
}
