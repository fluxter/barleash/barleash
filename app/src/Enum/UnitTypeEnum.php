<?php

namespace App\Enum;

enum UnitTypeEnum: string
{
    case Milliliter = 'ml';
}
