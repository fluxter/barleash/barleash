<?php

namespace App\DataFixtures;

use App\Factory\BarFactory;
use App\Factory\EventFactory;
use App\Factory\IngredientFactory;
use App\Factory\OrderFactory;
use App\Factory\OrderItemFactory;
use App\Factory\ParticipantFactory;
use App\Factory\RecipeFactory;
use App\Factory\RecipeIngredientFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $bar = BarFactory::createOne();

        $ingredients = IngredientFactory::new()
            ->withBar($bar)
            ->many(10, 30)
            ->create();

        $recipes = RecipeFactory::new()
            ->withBar($bar)
            ->many(5, 10)
            ->create();

        foreach ($recipes as $recipe) {
            RecipeIngredientFactory::new()
                ->withBar($bar)
                ->withAttributes([
                    'recipe' => $recipe,
                    'ingredient' => RecipeIngredientFactory::faker()->randomElement($ingredients),
                ])
                ->many(1, 3)
                ->create();
        }

        $event = EventFactory::new()
            ->withBar($bar)
            ->create();

        $participants = ParticipantFactory::new()
            ->withBar($bar)
            ->withAttributes([
                'event' => $event,
            ])
            ->many(5, 10)
            ->create();

        $orders = OrderFactory::new()
            ->withBar($bar)
            ->withAttributes([
                'event' => $event,
                'participant' => ParticipantFactory::faker()->randomElement($participants),
            ])
            ->many(5, 10)
            ->create();
        foreach ($orders as $order) {
            OrderItemFactory::new()
                ->withBar($bar)
                ->withAttributes([
                    'recipe' => $bar->getRecipes()[0],
                    'quantity' => 1,
                    'order' => $order,
                ])
                ->create();
        }
    }
}
