<?php

namespace App\Repository;

use App\Entity\Bar;
use App\Entity\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Order>
 *
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    public function getNextOrderNumberByBar(Bar $bar): int
    {
        $qb = $this->createQueryBuilder('o');
        $qb->select('MAX(o.orderNumber) as maxOrderNumber')
            ->andWhere('o.bar = :bar')
            ->andWhere(
                $qb->expr()->between(
                    'o.createdAt',
                    ':startOfDay',
                    ':endOfDay'
                )
            )
            ->setParameter('bar', $bar)
            ->setParameter('startOfDay', new \DateTime('today midnight'))
            ->setParameter('endOfDay', new \DateTime('today 23:59:59'));
        $result = $qb->getQuery()->getSingleScalarResult() ?? 1000;

        return $result + 1;
    }
}
