<?php

namespace App\Repository\Abstraction;

use App\Entity\Abstract\AbstractEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

abstract class AbstractRepository extends ServiceEntityRepository
{
    public function save(AbstractEntity $entity): void
    {
        $this->_em->persist($entity);
        $this->_em->flush();
    }
}
