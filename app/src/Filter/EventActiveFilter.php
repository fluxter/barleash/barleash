<?php

namespace App\Filter;

use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use Doctrine\ORM\QueryBuilder;

class EventActiveFilter extends AbstractFilter
{
    public function apply(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, Operation $operation = null, array $context = []): void
    {
        if ($context['filters']['active'] ?? null == null) {
            return;
        }

        $active = boolval($context['filters']['active']);

        $rootAlias = $queryBuilder->getRootAliases()[0];
        $nowParam = $queryNameGenerator->generateParameterName('now');
        $queryBuilder
            ->andWhere(sprintf('%s.start %s= :%s', $rootAlias, $active ? '>' : '<', $nowParam))
            ->andWhere(sprintf('%s.end %s= :%s', $rootAlias, $active ? '<' : '>', $nowParam))
            ->setParameter($nowParam, new \DateTime());
    }

    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, Operation $operation = null, array $context = []): void
    {
    }

    public function getDescription(string $resourceClass): array
    {
        return [
            'active' => [
                'type' => 'boolean',
                'property' => 'active',
                'required' => false,
            ],
        ];
    }
}
