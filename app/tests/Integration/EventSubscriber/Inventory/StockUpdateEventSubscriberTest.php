<?php

namespace App\Tests\Integration\EventSubscriber\Inventory;

use App\Enum\OrderStatusEnum;
use App\Factory\IngredientFactory;
use App\Factory\OrderFactory;
use App\Factory\OrderItemFactory;
use App\Factory\RecipeFactory;
use App\Factory\RecipeIngredientFactory;
use App\Tests\Integration\AbstractIntegrationTest;

class StockUpdateEventSubscriberTest extends AbstractIntegrationTest
{
    public function testStockUpdate(): void
    {
        // Prepare
        self::bootKernel();

        // Data
        $ingredients = IngredientFactory::new()
            ->withAttributes(['stock' => 100])
            ->many(3)
            ->create();
        $recipe = RecipeFactory::createOne([
            'ingredients' => [
                RecipeIngredientFactory::createOne([
                    'quantity' => 1,
                    'ingredient' => $ingredients[0],
                ]),
                RecipeIngredientFactory::createOne([
                    'quantity' => 2,
                    'ingredient' => $ingredients[1],
                ]),
                RecipeIngredientFactory::createOne([
                    'quantity' => 3,
                    'ingredient' => $ingredients[2],
                ]),
            ],
        ]);
        $orders = [
            OrderFactory::createOne([
                'status' => OrderStatusEnum::Pending,
                'orderItems' => [
                    OrderItemFactory::createOne([
                        'recipe' => $recipe,
                        'quantity' => 1,
                    ]),
                    OrderItemFactory::createOne([
                        'recipe' => $recipe,
                        'quantity' => 2,
                    ]),
                ],
            ]),
            OrderFactory::createOne([
                'status' => OrderStatusEnum::Pending,
                'orderItems' => [
                    OrderItemFactory::createOne([
                        'recipe' => $recipe,
                        'quantity' => 1,
                    ]),
                    OrderItemFactory::createOne([
                        'recipe' => $recipe,
                        'quantity' => 2,
                    ]),
                ],
            ])
        ];

        // Test
        static::assertSame($ingredients[0]->getStock(), 100);
        static::assertSame($ingredients[1]->getStock(), 100);
        static::assertSame($ingredients[2]->getStock(), 100);

        $orders[0]->setStatus(OrderStatusEnum::Completed);
        $orders[0]->save();

        static::assertSame($ingredients[0]->getStock(), 97);
        static::assertSame($ingredients[1]->getStock(), 94);
        static::assertSame($ingredients[2]->getStock(), 91);
    }
}
