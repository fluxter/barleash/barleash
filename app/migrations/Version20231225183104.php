<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231225183104 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE recipe_category (id UUID NOT NULL, bar_id UUID NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, name VARCHAR(128) NOT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_70DCBC5F89A253A ON recipe_category (bar_id)');
        $this->addSql('COMMENT ON COLUMN recipe_category.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN recipe_category.bar_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE recipe_category ADD CONSTRAINT FK_70DCBC5F89A253A FOREIGN KEY (bar_id) REFERENCES bar (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE recipe ADD recipe_category_id UUID NOT NULL');
        $this->addSql('COMMENT ON COLUMN recipe.recipe_category_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE recipe ADD CONSTRAINT FK_DA88B137C6B4D386 FOREIGN KEY (recipe_category_id) REFERENCES recipe_category (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_DA88B137C6B4D386 ON recipe (recipe_category_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE recipe DROP CONSTRAINT FK_DA88B137C6B4D386');
        $this->addSql('ALTER TABLE recipe_category DROP CONSTRAINT FK_70DCBC5F89A253A');
        $this->addSql('DROP TABLE recipe_category');
        $this->addSql('DROP INDEX IDX_DA88B137C6B4D386');
        $this->addSql('ALTER TABLE recipe DROP recipe_category_id');
    }
}
