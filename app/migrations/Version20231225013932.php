<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231225013932 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE inventory_item DROP CONSTRAINT fk_55bdea30933fe08c');
        $this->addSql('ALTER TABLE inventory_item DROP CONSTRAINT fk_55bdea3089a253a');
        $this->addSql('DROP TABLE inventory_item');
        $this->addSql('ALTER TABLE ingredient ADD stock INT NOT NULL');
        $this->addSql('ALTER TABLE recipe_ingredient ADD quantity INT NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE TABLE inventory_item (id UUID NOT NULL, ingredient_id UUID NOT NULL, bar_id UUID NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, stock INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_55bdea3089a253a ON inventory_item (bar_id)');
        $this->addSql('CREATE INDEX idx_55bdea30933fe08c ON inventory_item (ingredient_id)');
        $this->addSql('COMMENT ON COLUMN inventory_item.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN inventory_item.ingredient_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN inventory_item.bar_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE inventory_item ADD CONSTRAINT fk_55bdea30933fe08c FOREIGN KEY (ingredient_id) REFERENCES ingredient (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE inventory_item ADD CONSTRAINT fk_55bdea3089a253a FOREIGN KEY (bar_id) REFERENCES bar (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE recipe_ingredient DROP quantity');
        $this->addSql('ALTER TABLE ingredient DROP stock');
    }
}
