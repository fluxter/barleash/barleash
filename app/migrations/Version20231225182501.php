<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231225182501 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE event (id UUID NOT NULL, bar_id UUID NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, name VARCHAR(64) NOT NULL, start DATE NOT NULL, "end" DATE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_3BAE0AA789A253A ON event (bar_id)');
        $this->addSql('COMMENT ON COLUMN event.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN event.bar_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE participant (id UUID NOT NULL, bar_id UUID NOT NULL, event_id UUID NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, name VARCHAR(255) NOT NULL, claimed BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D79F6B1189A253A ON participant (bar_id)');
        $this->addSql('CREATE INDEX IDX_D79F6B1171F7E88B ON participant (event_id)');
        $this->addSql('COMMENT ON COLUMN participant.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN participant.bar_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN participant.event_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA789A253A FOREIGN KEY (bar_id) REFERENCES bar (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE participant ADD CONSTRAINT FK_D79F6B1189A253A FOREIGN KEY (bar_id) REFERENCES bar (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE participant ADD CONSTRAINT FK_D79F6B1171F7E88B FOREIGN KEY (event_id) REFERENCES event (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "order" ADD event_id UUID NOT NULL');
        $this->addSql('ALTER TABLE "order" ADD participant_id UUID NOT NULL');
        $this->addSql('ALTER TABLE "order" DROP name');
        $this->addSql('COMMENT ON COLUMN "order".event_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN "order".participant_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE "order" ADD CONSTRAINT FK_F529939871F7E88B FOREIGN KEY (event_id) REFERENCES event (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "order" ADD CONSTRAINT FK_F52993989D1C3019 FOREIGN KEY (participant_id) REFERENCES participant (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_F529939871F7E88B ON "order" (event_id)');
        $this->addSql('CREATE INDEX IDX_F52993989D1C3019 ON "order" (participant_id)');
        $this->addSql('ALTER TABLE order_item ALTER bar_id SET NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE "order" DROP CONSTRAINT FK_F529939871F7E88B');
        $this->addSql('ALTER TABLE "order" DROP CONSTRAINT FK_F52993989D1C3019');
        $this->addSql('ALTER TABLE event DROP CONSTRAINT FK_3BAE0AA789A253A');
        $this->addSql('ALTER TABLE participant DROP CONSTRAINT FK_D79F6B1189A253A');
        $this->addSql('ALTER TABLE participant DROP CONSTRAINT FK_D79F6B1171F7E88B');
        $this->addSql('DROP TABLE event');
        $this->addSql('DROP TABLE participant');
        $this->addSql('ALTER TABLE order_item ALTER bar_id DROP NOT NULL');
        $this->addSql('DROP INDEX IDX_F529939871F7E88B');
        $this->addSql('DROP INDEX IDX_F52993989D1C3019');
        $this->addSql('ALTER TABLE "order" ADD name VARCHAR(128) NOT NULL');
        $this->addSql('ALTER TABLE "order" DROP event_id');
        $this->addSql('ALTER TABLE "order" DROP participant_id');
    }
}
