<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231222225725 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE "order" (id UUID NOT NULL, bar_id UUID NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, name VARCHAR(128) NOT NULL, order_number INT NOT NULL, status VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_F529939889A253A ON "order" (bar_id)');
        $this->addSql('COMMENT ON COLUMN "order".id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN "order".bar_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE bar (id UUID NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, name VARCHAR(64) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN bar.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE ingredient (id UUID NOT NULL, bar_id UUID DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, name VARCHAR(255) NOT NULL, alcohol_percent INT NOT NULL, unit VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6BAF787089A253A ON ingredient (bar_id)');
        $this->addSql('COMMENT ON COLUMN ingredient.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN ingredient.bar_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE inventory_item (id UUID NOT NULL, ingredient_id UUID NOT NULL, bar_id UUID NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, stock INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_55BDEA30933FE08C ON inventory_item (ingredient_id)');
        $this->addSql('CREATE INDEX IDX_55BDEA3089A253A ON inventory_item (bar_id)');
        $this->addSql('COMMENT ON COLUMN inventory_item.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN inventory_item.ingredient_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN inventory_item.bar_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE order_item (id UUID NOT NULL, recipe_id UUID NOT NULL, order_id UUID NOT NULL, bar_id UUID DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, count INT NOT NULL, notes VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_52EA1F0959D8A214 ON order_item (recipe_id)');
        $this->addSql('CREATE INDEX IDX_52EA1F098D9F6D38 ON order_item (order_id)');
        $this->addSql('CREATE INDEX IDX_52EA1F0989A253A ON order_item (bar_id)');
        $this->addSql('COMMENT ON COLUMN order_item.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN order_item.recipe_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN order_item.order_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN order_item.bar_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE recipe (id UUID NOT NULL, bar_id UUID NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, name VARCHAR(128) NOT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_DA88B13789A253A ON recipe (bar_id)');
        $this->addSql('COMMENT ON COLUMN recipe.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN recipe.bar_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE recipe_ingredient (id UUID NOT NULL, recipe_id UUID NOT NULL, ingredient_id UUID DEFAULT NULL, bar_id UUID NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_22D1FE1359D8A214 ON recipe_ingredient (recipe_id)');
        $this->addSql('CREATE INDEX IDX_22D1FE13933FE08C ON recipe_ingredient (ingredient_id)');
        $this->addSql('CREATE INDEX IDX_22D1FE1389A253A ON recipe_ingredient (bar_id)');
        $this->addSql('COMMENT ON COLUMN recipe_ingredient.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN recipe_ingredient.recipe_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN recipe_ingredient.ingredient_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN recipe_ingredient.bar_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE "order" ADD CONSTRAINT FK_F529939889A253A FOREIGN KEY (bar_id) REFERENCES bar (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ingredient ADD CONSTRAINT FK_6BAF787089A253A FOREIGN KEY (bar_id) REFERENCES bar (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE inventory_item ADD CONSTRAINT FK_55BDEA30933FE08C FOREIGN KEY (ingredient_id) REFERENCES ingredient (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE inventory_item ADD CONSTRAINT FK_55BDEA3089A253A FOREIGN KEY (bar_id) REFERENCES bar (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_item ADD CONSTRAINT FK_52EA1F0959D8A214 FOREIGN KEY (recipe_id) REFERENCES recipe (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_item ADD CONSTRAINT FK_52EA1F098D9F6D38 FOREIGN KEY (order_id) REFERENCES "order" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_item ADD CONSTRAINT FK_52EA1F0989A253A FOREIGN KEY (bar_id) REFERENCES bar (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE recipe ADD CONSTRAINT FK_DA88B13789A253A FOREIGN KEY (bar_id) REFERENCES bar (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE recipe_ingredient ADD CONSTRAINT FK_22D1FE1359D8A214 FOREIGN KEY (recipe_id) REFERENCES recipe (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE recipe_ingredient ADD CONSTRAINT FK_22D1FE13933FE08C FOREIGN KEY (ingredient_id) REFERENCES ingredient (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE recipe_ingredient ADD CONSTRAINT FK_22D1FE1389A253A FOREIGN KEY (bar_id) REFERENCES bar (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE "order" DROP CONSTRAINT FK_F529939889A253A');
        $this->addSql('ALTER TABLE ingredient DROP CONSTRAINT FK_6BAF787089A253A');
        $this->addSql('ALTER TABLE inventory_item DROP CONSTRAINT FK_55BDEA30933FE08C');
        $this->addSql('ALTER TABLE inventory_item DROP CONSTRAINT FK_55BDEA3089A253A');
        $this->addSql('ALTER TABLE order_item DROP CONSTRAINT FK_52EA1F0959D8A214');
        $this->addSql('ALTER TABLE order_item DROP CONSTRAINT FK_52EA1F098D9F6D38');
        $this->addSql('ALTER TABLE order_item DROP CONSTRAINT FK_52EA1F0989A253A');
        $this->addSql('ALTER TABLE recipe DROP CONSTRAINT FK_DA88B13789A253A');
        $this->addSql('ALTER TABLE recipe_ingredient DROP CONSTRAINT FK_22D1FE1359D8A214');
        $this->addSql('ALTER TABLE recipe_ingredient DROP CONSTRAINT FK_22D1FE13933FE08C');
        $this->addSql('ALTER TABLE recipe_ingredient DROP CONSTRAINT FK_22D1FE1389A253A');
        $this->addSql('DROP TABLE "order"');
        $this->addSql('DROP TABLE bar');
        $this->addSql('DROP TABLE ingredient');
        $this->addSql('DROP TABLE inventory_item');
        $this->addSql('DROP TABLE order_item');
        $this->addSql('DROP TABLE recipe');
        $this->addSql('DROP TABLE recipe_ingredient');
    }
}
