migration:
	cd app && \
		bin/console doctrine:database:drop --force || true && \
		bin/console doctrine:database:create && \
		bin/console doctrine:migrations:migrate --no-interaction && \
		bin/console doctrine:migrations:diff && \
		bin/console doctrine:migrations:migrate --no-interaction

fixtures:
	cd app && \
		bin/console doctrine:schema:drop --force || true && \
		bin/console doctrine:schema:create && \
		bin/console doctrine:schema:update --force --complete && \
		bin/console doctrine:fixtures:load --no-interaction 

entities:
	cd app && \
		bin/console make:entity --regenerate --no-interaction App\\Entity\\